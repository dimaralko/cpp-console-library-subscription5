#ifndef USE_INTERNET_H
#define USE_INTERNET_H

#include "constants.h"

struct times
{
    int hour;
    int min;
    int sec;
};

struct program
{
    char name[MAX_STRING_SIZE];
};
struct memoryes {
    int byte1;
    int byte2;
};
struct use_internet
{
    times start;
    times finish;
    memoryes bytes;
    char name[MAX_STRING_SIZE];
};

#endif
