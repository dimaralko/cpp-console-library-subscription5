#include <iostream>
#include <iomanip>
#include "file_reader.h"
#include "constants.h"

using namespace std;

int main()
{

    cout << "Laboratory work #8. GIT\n";
    cout << "Variant #5. Library Subscription\n";
    cout << "Author: Dmitriy Ralko\n";
    cout << "Group: 12\n";
    use_internet* subscriptions[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", subscriptions, size);
        cout << "***** Протокол работы в Интернет *****\n\n";
        for (int i = 0; i < size; i++)
        {
            cout << "Начало работы программы........:";
            cout << setw(2) << setfill('0') << subscriptions[i]->start.hour << '\n';
            cout << setw(2) << setfill('0') << subscriptions[i]->start.min << '\n';
            cout << setw(2) << setfill('0') << subscriptions[i]->start.sec << '\n';
            cout << '\n';
            cout << "Конец работы программы........:";
            cout << subscriptions[i]->finish.hour << ' ';
            cout << subscriptions[i]->finish.min << ' ';
            cout << subscriptions[i]->finish.sec << '\n';
            cout << "Кол-во байтов получено.........:";
            cout << subscriptions[i]->bytes.byte1 << ' ';
            cout << '\n';
            cout << "Кол-во байтов отправлено.........:";
            cout << subscriptions[i]->bytes.byte2 << ' ';
            cout << '\n';
            cout << "Название программы.........:";
            cout << subscriptions[i]->name << '\n';
            cout << '\n';
        }
        for (int i = 0; i < size; i++)
        {
            delete subscriptions[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}
