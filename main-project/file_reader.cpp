#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>

times convert(char* str)
{
    times result;
    char* context = NULL;
    char* str_number = strtok_s(str, ".", &context);
    result.hour = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.min = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.sec = atoi(str_number);
    return result;
}

void read(const char* file_name, use_internet* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            use_internet* item = new use_internet;
            file >> item->start.hour;
            file >> item->start.min;
            file >> item->start.sec;
            file >> tmp_buffer;
            item->bytes.byte1;
            file >> tmp_buffer;
            item->bytes.byte2;
            file.read(tmp_buffer, 1); // чтения лишнего символа пробела
            file.getline(item->name, MAX_STRING_SIZE);
            array[size++] = item;
        }
        file.close();
    }
    else
    {
        throw "Ошибка открытия файла";
    }
}
