#ifndef FILE_READER_H
#define FILE_READER_H

#include "use_internet.h"

void read(const char* file_name, use_internet* array[], int& size);

#endif
